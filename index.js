const express = require('express');

const PORT = process.env.PORT || 3000;

const app = express();

app.get('/', (req, res) => {
  res.send('Hello world, welcome to the universe');
});

app.listen({ port: PORT }, () => {
  console.log('Listen at', PORT)
});
